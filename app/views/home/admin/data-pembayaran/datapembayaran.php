<!-- Begin Page Content -->
<div class="container-fluid" style="text-align: center;">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
</div>

<!-- Content Row -->
<div class="row">

<!-- Begin Page Content -->
<div class="container-fluid" style="text-align: center;">

<!-- Content Row -->
<div class="row">
    
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Pembayaran</h6>
    </div>
    <div class="card-body">
    <div class="" style="text-align: left;">
    <button type="button" class="btn btn-success" style="margin-bottom: 20px;" data-toggle="modal" data-target="#createPembayaran">
        Tambah Data
    </button>
    </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tahun Ajaran</th>
                        <th>Nominal</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['pembayaran'] as $pembayaran): ?>
                    <tr>
                        <td><?=$pembayaran['id']?></td>
                        <td><?=$pembayaran['tahun_ajaran']?></td>
                        <td><?=$pembayaran['nominal']?></td>
                        <td>
                        <a href="<?=BASE_URL?>admin_pembayaran/edit/<?=$pembayaran['id']?>" class="btn btn-primary">Edit</a>
                        <a href="<?=BASE_URL?>admin_pembayaran/delete/<?=$pembayaran['id']?>" class="btn btn-danger" onclick="return confirm('yakin?')">Hapus</a>
                        </td>
                    </tr>
                    <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>


</div>

</div>
<!-- /.container-fluid -->

<!-- Modal Tambah  Data -->
<div class="modal fade" id="createPembayaran" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
<form class="user" method="POST" action="<?=BASE_URL;?>admin_pembayaran/prosesTambah">

<div class="modal-content">
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pembayaran :</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
<div class="form-group">
    <input type="text" class="form-control"
            id="tahun_ajaran" name="tahun_ajaran" aria-describedby="emailHelp"
            placeholder="Masukkan Tahun Ajaran..">
</div>
<div class="form-group">
    <input type="text" class="form-control"
            id="nominal" name="nominal" aria-describedby="emailHelp"
            placeholder="Masukkan Nominal..">
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Simpan Data</button>
</div>
</div>
</div>
</form>
</div>
</div>
