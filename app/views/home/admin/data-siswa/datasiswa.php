<!-- Begin Page Content -->
<div class="container-fluid" style="text-align: center;">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
</div>

<!-- Content Row -->
<div class="row">
    
<!-- Begin Page Content -->
<div class="container-fluid" style="text-align: center;">

    <!-- Content Row -->
    <div class="row">
    
    <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Siswa</h6>
        </div>
        <div class="card-body" style="text-align: left;">
        <button type="button" class="btn btn-success" style="margin-bottom: 20px;" data-toggle="modal" data-target="#createSiswa">
            Tambah Data
        </button>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NISN</th>
                            <th>NIS</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>No Telepon</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['siswa'] as $siswa):?>
                        <tr>
                            <td><?=$siswa['id']?></td>
                            <td style="text-align: left;"><?=$siswa['nisn']?></td>
                            <td style="text-align: left;"><?=$siswa['nis']?></td>
                            <td style="text-align: left;"><?=$siswa['nama']?></td>
                            <td style="text-align: left;"><?=$siswa['alamat']?></td>
                            <td style="text-align: left;"><?=$siswa['telepon']?></td>
                            <td>
                            <a href="<?=BASE_URL?>/admin_siswa/edit/<?=$siswa['id']?>" class="btn btn-primary">Edit</a>
                            <a href="<?=BASE_URL?>/admin_siswa/delete/<?=$siswa['id']?>" class="btn btn-danger" onclick="return confirm('yakin?')">Hapus</a>
                            </td>
                        </tr>
                        <?php endforeach?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


    </div>

</div>
<!-- /.container-fluid -->

<!-- Modal Tambah  Data -->
<div class="modal fade" id="createSiswa" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
<form class="user" method="POST" action="<?=BASE_URL;?>/admin_siswa/tambahSiswa">

    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pembayaran :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
    <div class="form-group">
        <input type="text" class="form-control"
                id="nisn" name="nisn" aria-describedby="emailHelp"
                placeholder="Masukkan NISN..">
    </div>
    <div class="form-group">
        <input type="text" class="form-control"
                id="nis" name="nis" aria-describedby="emailHelp"
                placeholder="Masukkan NIS..">
    </div>
    <div class="form-group">
        <input type="text" class="form-control"
                id="nama" name="nama" aria-describedby="emailHelp"
                placeholder="Masukkan Nama..">
    </div>
    <div class="form-group">
        <input type="text" class="form-control"
                id="alamat" name="alamat" aria-describedby="emailHelp"
                placeholder="Masukkan Alamat..">
    </div>
    <div class="form-group">
        <input type="text" class="form-control"
                id="telepon" name="telepon" aria-describedby="emailHelp"
                placeholder="Masukkan Telepon..">
    </div>
    <div class="form-group">
        <label for="example">Kelas ID</label>
        <select name="kelas_id" class="form-control">
            <?php foreach($data['kelas'] as $kelas):?>
            <option value="<?=$kelas['id']?>"><?=$kelas['nama']?> - <?=$kelas['kompetensi_keahlian']?></option>
            <?php endforeach?>
        </select>
    </div>

    <div class="form-group">
        <label for="example">Pengguna ID</label>
        <select name="pengguna_id" class="form-control">
            <?php foreach($data['pengguna'] as $pengguna):?>
            <option value="<?=$pengguna['id']?>"><?=$pengguna['role']?></option>
            <?php endforeach?>
        </select>
    </div>

    <div class="form-group">
        <label for="example">Pembayaran ID</label>
        <select name="pembayaran_id" class="form-control">
            <?php foreach($data['pembayaran'] as $pembayaran):?>
            <option value="<?=$pembayaran['id']?>"><?=$pembayaran['tahun_ajaran']?> - <?=$pembayaran['nominal']?></option>
            <?php endforeach?>
        </select>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan Data</button>
    </div>
    </div>
    </div>
    </form>
</div>
</div>
