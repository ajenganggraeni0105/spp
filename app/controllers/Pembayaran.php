<?php

class Pembayaran extends Controller {
    public function index () {
        $data["Judul"] = "Pembayaran";
        $data["pembayaran"] = $this->model("Pembayaran_model")->getAllPembayaran();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/datapembayaran", $data);
        $this->view("templates/footer");
    }
}