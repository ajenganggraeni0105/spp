<?php

class Admin_kelas extends Controller {
    public function index() {
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $this->view('home/admin/data-kelas/datakelas', $data);
    }

    public function prosesTambah() {
        if($this->model('Kelas_model')->createKelas($_POST) > 0) {
            header('Location:' . BASE_URL . 'admin/kelas');
        }
    }

    public function edit($id) {
        $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("templates/footer");
        $this->view('home/admin/data-kelas/edit-kelas', $data);
    }

    public function prosesUpdate($id) {
        if($this->model('Kelas_model')->updateKelas($_POST) > 0) {
            header('Location:' . BASE_URL . 'admin/kelas');
            exit;
        }
    }

    public function delete($id) {
        if($this->model('Kelas_model')->deleteKelas($id) > 0) {
            header('Location:' . BASE_URL . 'admin/kelas');
            exit;
        }
    }
}