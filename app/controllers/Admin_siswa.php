<?php
    class Admin_siswa extends Controller
    {
        public function index() {
            $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
            $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
            $this->view('home/admin/data-siswa/datasiswa', $data);
        }

        public function prosesTambah() {
            if($this->model('Siswa_model')->createSiswa($_POST) > 0) {
                header('Location:' . BASE_URL . 'admin/siswa');
            }
        }
    
        public function edit($id) {
            $data['siswa'] = $this->model('Siswa_model')->getSiswaById($id);
            $this->view("templates/header", $data);
            $this->view("templates/sidebar");
            $this->view("templates/footer");
            $this->view("home/admin/data-siswa/edit-siswa", $data);
        }
    
        public function prosesUpdate($id) {
            if($this->model('Siswa_model')->updateSiswa($_POST) > 0) {
                header('Location:' . BASE_URL . 'admin/siswa');
                exit;
            }
        }
    
        public function delete($id) {
            if($this->model('Siswa_model')->deleteSiswa($id) > 0) {
                header('Location:' . BASE_URL . 'admin/siswa');
                exit;
            }
        }
    }
