<?php

class Petugas extends Controller {
    public function index () {
        $data["Judul"] = "Petugas";
        $data["petugas"] = $this->model("Petugas_model")->getAllPetugas();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/datapetugas", $data);
        $this->view("templates/footer");
    }
}