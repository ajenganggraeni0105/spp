<?php

class Siswa extends Controller {
    public function index () {
        $data["Judul"] = "Siswa";
        $data["siswa"] = $this->model("Siswa_model")->getAllSiswa();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/datasiswa", $data);
        $this->view("templates/footer");
    }
}