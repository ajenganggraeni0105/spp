<?php

class Admin_pengguna extends Controller {
    public function index() {
        $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
        $this->view('home/admin/data-pengguna/datapengguna', $data);
    }

    public function prosesTambah() {
        if($this->model('Pengguna_model')->createPengguna($_POST) > 0) {
            header('Location:' . BASE_URL . 'admin/pengguna');
        }
    }

    public function edit($id) {
        $data['pengguna'] = $this->model('Pengguna_model')->getPenggunaById($id);
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("templates/footer");
        $this->view('home/admin/data-pengguna/edit-pengguna', $data);
    }

    public function prosesUpdate($id) {
        if($this->model('Pengguna_model')->updatePengguna($_POST) > 0) {
            header('Location:' . BASE_URL . 'admin/pengguna');
            exit;
        }
    }

    public function delete($id) {
        if($this->model('Pengguna_model')->deletePengguna($id) > 0) {
            header('Location:' . BASE_URL . 'admin/pengguna');
            exit;
        }
    }
}