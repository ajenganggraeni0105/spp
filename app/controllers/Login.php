<?php

class Login extends Controller {
    public function index() {
        if(isset($_SESSION['user'])) {
            header('Location: ' . BASE_URL);
            exit;
        }
        $this->view("auth/login");
    }
    
        public function prosesLogin() {
            $pengguna = $this->model('Pengguna_model')->getPenggunaByUsername($_POST['username']);
    
            if($this->model('Pengguna_model')->authPenggunaByUsernamePassword($_POST) > 0) {
                if ($pengguna['role'] == 'admin') {
                    header("Location: " . BASE_URL);
                } else if ($pengguna['role'] == 'petugas') {
                    echo "page petugas";
                } else if ($pengguna['role'] == 'siswa') {
                    echo "page siswa";
                } 
            }

            // else if($this->model('Siswa_model')->authSiswaByUsername($_POST) > 0) {
            //     $siswa = $this->model('Siswa_model')->getSiswaByUsername($_POST['username']);
            //     header('Location: ' . BASE_URL . 'home/siswa');
            // }
        }
    }

    