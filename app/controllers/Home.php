<?php
	
	class Home extends Controller{
		public function index(){
			$data['judul'] = 'Home';
			$this->view('templates/header', $data);
			$this->view('templates/sidebar');
			$this->view('home/admin/index', $data);
			$this->view('templates/footer');
		}
	}
	