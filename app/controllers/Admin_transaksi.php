<?php

    class Admin_Transaksi extends Controller {
        public function index() {
            $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
            $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
            $data['petugas'] = $this->model('Petugas_model')->getAllPetugas();
            $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
            $this->view('home/admin/data-transaksi/transaksi', $data);
        }

    }
