<?php

class Admin extends Controller {
    public function index() {
        $data['judul'] = 'Home';
        $this->view('templates/header', $data);
        $this->view('templates/sidebar');
        $this->view('home/admin/datasiswa', $data);
        $this->view('templates/footer');
    }

    public function siswa() {
        $data["siswa"] = $this->model("Siswa_model")->getAllSiswa();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/data-siswa/datasiswa", $data);
        $this->view("templates/footer");
    }

    public function petugas() {
        $data["petugas"] = $this->model("Petugas_model")->getAllPetugas();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/data-petugas/datapetugas", $data);
        $this->view("templates/footer");
    }

    public function pengguna() {
        $data["pengguna"] = $this->model("Pengguna_model")->getAllPengguna();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/data-pengguna/datapengguna", $data);
        $this->view("templates/footer");
    }

    public function kelas() {
        $data["kelas"] = $this->model("Kelas_model")->getAllKelas();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/data-kelas/datakelas", $data);
        $this->view("templates/footer");
    }

    public function pembayaran() {
        $data["pembayaran"] = $this->model("Pembayaran_model")->getAllPembayaran();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/data-pembayaran/datapembayaran", $data);
        $this->view("templates/footer");
    }

    public function transaksi() {
        $data["transaksi"] = $this->model("Transaksi_model")->getAllTransaksi();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/data-transaksi/datatransaksi", $data);
        $this->view("templates/footer");
    }

}