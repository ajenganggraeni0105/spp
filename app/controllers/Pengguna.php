<?php

class Pengguna extends Controller {
    public function index () {
        $data["Judul"] = "Pengguna";
        $data["pengguna"] = $this->model("Pengguna_model")->getAllPengguna();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/datapengguna", $data);
        $this->view("templates/footer");
    }
}