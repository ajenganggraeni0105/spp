<?php

class Admin_petugas extends Controller {
    public function index() {
        $data['petugas'] = $this->model('Petugas_model')->getAllPetugas();
        $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
        $this->view('home/admin/data-petugas/datapetugas', $data);
    }

    public function prosesTambah() {
        if($this->model('Petugas_model')->createPetugas($_POST) > 0) {
            header('Location:' . BASE_URL . 'admin/petugas');
        }
    }

    public function edit($id) {
        $data['petugas'] = $this->model('Petugas_model')->getPetugasById($id);
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("templates/footer");
        $this->view('home/admin/data-petugas/edit-petugas', $data);
    }

    public function prosesUpdate($id) {
        if($this->model('Petugas_model')->updatePetugas($_POST) > 0) {
            header('Location:' . BASE_URL . 'admin/petugas');
            exit;
        }
    }

    public function delete($id) {
        if($this->model('Petugas_model')->deletePetugas($id) > 0) {
            header('Location:' . BASE_URL . 'admin/petugas');
            exit;
        }
    }
}