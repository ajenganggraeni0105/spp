<?php

class Kelas extends Controller {
    public function index () {
        $data["Judul"] = "Kelas";
        $data["kelas"] = $this->model("Kelas_model")->getAllKelas();
        $this->view("templates/header", $data);
        $this->view("templates/sidebar");
        $this->view("home/admin/datakelas", $data);
        $this->view("templates/footer");
    }
}