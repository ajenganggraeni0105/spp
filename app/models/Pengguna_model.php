<?php

class Pengguna_model {
    private $table = 'pengguna';
    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    public function getAllPengguna() {
        $this->db->query("SELECT * FROM  $this->table");
        return $this->db->resultAll();
    }

    public function getPenggunaByUsername($username) {
        $this->db->query("SELECT * FROM {$this->table} WHERE username = :username");
        $this->db->bind("username", $data);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function authPenggunaByUsername($data) {
        $this->db->query("SELECT * FROM {$this->table} WHERE username = :username AND password");
        $this->db->bind("password", $data['password']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getPenggunaById($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id = :id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function createPengguna($data) {
        $this->db->query("INSERT INTO {$this->table} VALUES (NULL, :username, :password, :role)");
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $data['password']);
        $this->db->bind('role', $data['role']);
        return $this->db->rowCount();
    }

    public function updatePengguna($data) {
        $this->db->query("UPDATE {$this->table} SET username = :username, password = :password, role = :role WHERE id = :id");
        $this->db->bind('id', $data['id']);
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $data['password']);
        $this->db->bind('role', $data['role']);
        return $this->db->rowCount();
    }

    public function deletePengguna($id){
        $this->db->query("DELETE FROM {$this->table} WHERE id = :id");
        $this->db->bind('id', $id);
        return $this->db->rowCount();
    }

}