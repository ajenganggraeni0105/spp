<?php

class Petugas_model {
    private $table = 'petugas';
    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    public function getAllPetugas() {
        $this->db->query("SELECT * FROM  $this->table");
        return $this->db->resultAll();
    }

    public function getPetugasByUsername($username) {
        $this->db->query("SELECT * FROM {$this->table} WHERE username = :username");
        $this->db->bind("username", $data);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function authPetugasByUsername($data) {
        $this->db->query("SELECT * FROM {$this->table} WHERE username = :username AND password");
        $this->db->bind("password", $data['password']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getPetugasById($id) {
        $query = "SELECT * FROM $this->table WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function createPetugas($data) {
        $query = "INSERT INTO {$this->table} VALUES(NULL, :nama, :pengguna_id)";
        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('pengguna_id', $data['pengguna_id']);
        return $this->db->rowCount();
    }

    public function updatePetugas($data) {
        $query = "UPDATE {$this->table} SET nama = :nama, pengguna_id = :pengguna_id WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $data['id']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('pengguna_id', $data['pengguna_id']);
        return $this->db->rowCount();
    }

    public function deletePetugas($id) {
        $query = "DELETE FROM {$this->table} WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        return $this->db->rowCount();
    }

}