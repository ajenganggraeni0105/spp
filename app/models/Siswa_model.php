<?php
    class Siswa_model
    {
        private $table = 'siswa';
        private $db;

        public function __construct()
        {
            $this->db = new Database;
        }

        public function getAllSiswa()
        {
            $this->db->query('SELECT * FROM ' . $this->table);
            return $this->db->resultAll();
        }

        public function getSiswa($username, $password)
        {
            $this->db->query("SELECT * FROM getsiswa WHERE username = :username AND password = :password");
            $this->db->bind('username', $username);
            $this->db->bind('password', $password);
            return $this->db->resultSingle();
        }

        public function getSiswaEntry()
        {
            $this->db->query("SELECT * FROM getsiswa");
            return $this->db->resultAll();
        }

        public function getSiswaById($id)
        {
            $this->db->query("SELECT * FROM {$this->table} WHERE id = :id");
            $this->db->bind('id', $id);
            return $this->db->resultSingle();
        }

        public function getSiswaByNisn($nisn)
        {
            $this->db->query("SELECT * FROM getsiswa WHERE nisn = :nisn");
            $this->db->bind('nisn', $nisn);
            return $this->db->resultSingle();
        }

        public function createSiswa($data)
        {
            $this->db->query("INSERT INTO {$this->table} VALUES(NULL, :nisn, :nis, :nama, :alamat, :telepon, :kelas_id, :pengguna_id, :pembayaran_id)");
            $this->db->bind('nisn', $data['nisn']);
            $this->db->bind('nis', $data['nis']);
            $this->db->bind('nama', $data['nama']);
            $this->db->bind('alamat', $data['alamat']);
            $this->db->bind('telepon', $data['telepon']);
            $this->db->bind('kelas_id', $data['kelas_id']);
            $this->db->bind('pengguna_id', $data['pengguna_id']);
            $this->db->bind('pembayaran_id', $data['pembayaran_id']);
            return $this->db->rowCount();
        }

        public function updateSiswa($data)
        {
            $this->db->query("UPDATE {$this->table} SET nisn = :nisn, nis = :nis, nama = :nama, alamat = :alamat, telepon = :telepon, kelas_id = :kelas_id, pengguna_id = :pengguna_id, pembayaran_id = :pembayaran_id WHERE id = :id");
            $this->db->bind('id', $data['id']);
            $this->db->bind('nisn', $data['nisn']);
            $this->db->bind('nis', $data['nis']);
            $this->db->bind('nama', $data['nama']);
            $this->db->bind('alamat', $data['alamat']);
            $this->db->bind('telepon', $data['telepon']);
            $this->db->bind('kelas_id', $data['kelas_id']);
            $this->db->bind('pengguna_id', $data['pengguna_id']);
            $this->db->bind('pembayaran_id', $data['pembayaran_id']);
            return $this->db->rowCount();
        }

        public function deleteSiswa($id)
        {
            $this->db->query("DELETE FROM {$this->table} WHERE id = :id");
            $this->db->bind('id', $id);
            return $this->db->rowCount();
        }

    }
