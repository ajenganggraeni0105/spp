<?php
    class Transaksi_model {
        private $table = 'transaksi';
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function getAllTransaksi() {
            $this->db->query("SELECT * FROM  $this->table");
            return $this->db->resultAll();
        }

    }
