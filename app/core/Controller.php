<?php 
	class Controller 
	{
		
		public function view($view, $data = [])    
		{
			require_once "../app/views/$view.php";   
		}

		public function model($model) // isi () = nama model yg mau dipake
		{
			require_once "../app/models/" . $model . ".php";
				return new $model;
		}
		
		// karena model = class, kita harus instansiasi dulu sblm dipake
		// makanya dgn $this->model(...) ==> panggil kelas model + instansiasi, jadi bisa dipake class nya

		// kalo udh dibuat function model nya, tinggal panggil di controllers ==> $this->model("..")->namaMethod();
	}